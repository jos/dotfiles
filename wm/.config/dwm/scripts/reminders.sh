#!/bin/sh

# this script shows a notification with the tasks for today,
# or prints the total number of tasks, if called with -l
# -q disables queuing timed reminders, since they will be handled separately
# -aa triggers timed reminders that haven't happened yet, but will omit the
# ones that have already passed
while getopts "ln" opt; do
    case ${opt} in
        n)
            notify-send -t 5000 -i /usr/share/icons/Papirus-Dark/24x24/actions/bell.svg "$(eval "remind -q -aa -ga $REMIND_DIR/reminders.rem | awk '/\\w/ {print}'")"
            ;;
        l)
            eval "remind -q -aa -ga $REMIND_DIR/reminders.rem | awk '/\\w/ {if (NR!=1) {print}}'" | wc -l
            ;;
        *)
            printf "%s\n" "Invalid option: $OPTARG" 1>&2
            ;;
    esac
done

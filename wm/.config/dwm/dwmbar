#!/bin/sh

. "${XDG_CONFIG_HOME:-$HOME/.config}/dwm/icons/icons_bar"

PANEL_FIFO="/tmp/panel.fifo"
DWM_SCRIPTS="${XDG_CONFIG_HOME:-$HOME/.config}/dwm/scripts"

# check for running instances
for pid in $(pidof -x "$(basename "$0")"); do
    if [ "$pid" != $$ ]; then
        printf "%s\n" "Already running." >&2
        exit 1
    fi
done

trap "rm -f $PANEL_FIFO" INT TERM QUIT EXIT

[ -e "$PANEL_FIFO" ] || mkfifo "$PANEL_FIFO"


# ========
# slstatus
# ========
# assumes that the slstatus output is 'cpu_perc ram_perc datetime'
slstatus -s | while read CPU MEM DATE; do
    [ "$CPU" -lt 60 ] && CPU="" || CPU="\037 ${CPU_ICON} ${CPU}% |"
    [ "$MEM" -lt 60 ] && MEM="" || MEM="\037 ${RAM_ICON} ${MEM}% |"
    CPU_RAM="CPU${CPU}${MEM}"
    DATE="DAT\005 ${DATE} |"
    printf "%b\n" "$CPU_RAM\n$DATE"
done > "$PANEL_FIFO" &


# =============
# time and date
# =============
# fallback if slstatus can't be found
[ ! -x "$(command -v slstatus)" ] && \
while :; do
    printf "%b\n" "DAT\005 $(date +'%a %d %b %H:%M') |"
    sleep 60
done > "$PANEL_FIFO" &


# ======
# volume
# ======
[ -x "$(command -v amixer)" ] && \
until [ "$(amixer)" ]; do sleep 0.1; done # wait until amixer is ready

[ -x "$(command -v pamixer)" ] && \
stdbuf -oL amixer events | while read -r line; do
    case ${line%%,*} in
        'event value: numid='[34])
            PULSE_VOLUME=$(pamixer --get-volume)
        
            if "$(pamixer --get-mute)"; then
                ICON="$MUTE"
            elif [ "$PULSE_VOLUME" -lt 25 ]; then
                ICON="$VOLUME_LOW"
            elif [ "$PULSE_VOLUME" -lt 50 ]; then
                ICON="$VOLUME_MID"
            else
                ICON="$VOLUME_HIGH"
            fi
        
            printf "%b\n" "VOL\004 ${ICON} ${PULSE_VOLUME}% |"
    esac
done > "$PANEL_FIFO" &


# =======
# battery
# =======
[ "$(grep -s Battery /sys/class/power_supply/*/type)" ] && \
while :; do
    # Update on plug/unplug (see misc/udev directory)
    # Check the capacity every 60 seconds otherwise
    [ -e "/tmp/batt_status" ] || touch "/tmp/batt_status"

    BAT_C=$(cat /sys/class/power_supply/BAT0/capacity)
    BAT_S=$(cat /sys/class/power_supply/BAT0/status)

    if [ "$BAT_S" = "Charging" ]; then
        ICON=$BATT_CHARGING
    elif [ "$BAT_C" -gt 20 ]; then
        ICON=$BATT_HIGH
    else
        ICON=$BATT_LOW
    fi

    printf "%b\n" "BAT\037 ${ICON} ${BAT_C}% |"

    if [ -x "$(command -v inotifywait)" ]; then
        inotifywait -qt 60 "/tmp/batt_status"
    else
        sleep 60
    fi
done > "$PANEL_FIFO" &


# ============
# arch updates
# ============
[ "$(hostnamectl | grep "Arch Linux")" ] && \
while :; do
    sleep 90 # Hopefully we are online after 90 seconds
    PAC=$(checkupdates 2> /dev/null | wc -l)
    AUR=$(paru -Qum 2> /dev/null | wc -l)
    UPDATES=$(( PAC + AUR ))

    if [ "$UPDATES" -eq 0 ]; then
        printf "%s\n" "UPD"
    else
        printf "%b\n" "UPD\002 ${PKG_ARCH} ${UPDATES} |"
    fi

    sleep 1110
done > "$PANEL_FIFO" &


# =========
# bluetooth
# =========
[ -n "$(rfkill list bluetooth)" ] && \
rfkill event | while read -r line; do

    # Returns 0 if bluetooth is enabled
    BT_ICON=$(rfkill list bluetooth | grep -c "yes")

    if [ "$BT_ICON" -eq 0 ]; then
        ICON="$BT_ON"
        BT_STATUS="on "
    else
        ICON="$BT_OFF"
        BT_STATUS="off"
    fi

    printf "%b\n" "BT\003 ${ICON} ${BT_STATUS} |"
done > "$PANEL_FIFO" &


# ============
# power button
# ============
printf "%b\n" "PWR\006 ${POWER_ICON} " > "$PANEL_FIFO" &


# ===============
# tasks for today
# ===============
[ -x "$(command -v remind)" ] && \
while :; do
    if [ -n "$(remind -q -aa -h $REMIND_DIR/reminders.rem)" ]; then
        NUMBER_TASKS=$("$DWM_SCRIPTS"/reminders.sh -l)
        printf "%b\n" "REM\001 ${BELL_ICON} ${NUMBER_TASKS} |"
    else
        printf "%s\n" "REM"
    fi

    sleep 1800
done > "$PANEL_FIFO" &


# =======
# weather
# =======
while :; do
    sleep 90 # Hopefully we are online after 90 seconds
    WEATHER=$("$DWM_SCRIPTS/weather.sh")
    printf "%b\n" "${WEATHER}"
    sleep 1710
done > "$PANEL_FIFO" &


# ===========
# scratchpads
# ===========
[ -x "$(command -v inotifywait)" ] && \
while :; do
    [ -f "/tmp/ishidden.wid" ] || touch "/tmp/ishidden.wid"
    SCRATCHPADS="$(wc -l < "/tmp/ishidden.wid")"
    if [ "$SCRATCHPADS" -eq 0 ]; then
        printf "%s\n" "PAD"
    else
        printf "%b\n" "PAD\007 ${PAD_ICON} ${SCRATCHPADS} |"
    fi
    inotifywait -qe modify /tmp/ishidden.wid
done > "$PANEL_FIFO" &


# ===========
# system tray
# ===========
# ask xprop about the window width, then draw 3 spaces per slot
# (the slot size is 20 as set in stalonetrayrc)
[ -x "$(command -v xprop)" ] && \
while :; do
    if pgrep -x stalonetray; then
        SLOT_SIZE=20
        xprop -name stalonetray -f WM_SIZE_HINTS 32i ' $5\n' \
            -spy WM_NORMAL_HINTS | while read -r dummy WIDTH; do
            TRAYBAR=$(printf "%*s" $(( (WIDTH / SLOT_SIZE) * 3 )) )
            printf "%b\n" "TRAY\037${TRAYBAR}"
        done
    else
        printf "%s\n" "TRAY"
        xdo id -N stalonetray -m || sleep 1
    fi
done > "$PANEL_FIFO" &


# ==============
# update the bar
# ==============
while IFS= read -r line; do
    case $line in
        CPU*)
            cpu="${line#???}"
            ;;
        REM*)
            rem="${line#???}"
            ;;
        UPD*)
            upd="${line#???}"
            ;;
        WTR*)
            wtr="${line#???}"
            ;;
        PAD*)
            pad="${line#???}"
            ;;
        BT*)
            bt="${line#??}"
            ;;
        VOL*)
            vol="${line#???}"
            ;;
        BAT*)
            bat="${line#???}"
            ;;
        DAT*)
            date="${line#???}"
            ;;
        TRAY*)
            tray="${line#????}"
            ;;
        PWR*)
            pwr="${line#???}"
            ;;
    esac 
        dwm -s "${cpu}${rem}${upd}${wtr}${pad}${bt}${vol}${bat}${date}${tray}${pwr}"
done < "$PANEL_FIFO"

# dotfiles

![pic](desktop.png)
![pic](desktop2.png)

Here's an overview of the different directories that exist in this repository.

```
 dunst        -> config file for the dunst notification-daemon
 git          -> git config, aliases and tag managemnt (in git-hooks)
 misc         -> mostly contains files that are no longer used
 mpv          -> video player settings and keyboard shortcuts
 music        -> configuration for mpd and the ncmpcpp client
 nsxiv        -> scripts that extend the nsxiv image viewer
 picom        -> configuration for the X11 compositor picom
 qutebrowser  -> keyboard-driven browser settings
 rofi         -> files that help using rofi as a power menu
 scripts      -> hopefully you will find something useful here
 shell        -> shell (bash) settings, aliases, etc.
 vifm         -> config for the ncurses-based file manager vifm
 vim          -> vim configuration
 wm           -> window manager, hotkey and status bar-related files
 x11          -> Xorg settings and color themes
 zathura      -> mappings for the zathura document viewer
```

I'm using [GNU Stow](https://www.gnu.org/software/stow/) to manage my files easily with the help of the install.sh script. Note that the script is meant for personal use only, and even though it won't delete any files (in case of conflict the original files will be renamed with the .bak extension), it's recommended to manually grab the piece of config/code you want instead.

If installing the dotfiles is desired, please continue reading:

The following sections will go through the steps for installing the dotfiles, which is a simple matter of running a script, as well as listing the "recommended" software and dependencies to (hopefully) have a system up and running with little to no pain on the user's side.

When you are ready, open the first section.
<details>
  <summary>Installation</summary>

  ## INSTALL PROCESS
  Install git if it isn't already, and clone the repository:
    
    $ git clone --depth=1 'https://codeberg.org/jos/dotfiles.git'

  Now, change the working directory:

    $ cd dotfiles/

  Inspect the PACKAGES array inside `install.sh` and remove any packages that you don't want installed.

  Finally, run the script that will take care of everything else:

    $ ./install.sh

  That's it! The last step would be cleaning up the bak files that could have been generated because of conflicts.
</details>

<details>
  <summary>Software</summary>

  ## RECOMMENDED SOFTWARE
  * [dwm](https://dwm.suckless.org/) ([my fork](https://codeberg.org/jos/dwm)) as the **window manager**. Very comfy once you get used to the concept of tags, although can be a bit overwhelming due to the abundance of patches and how different it is to other WMs. [This build](https://github.com/bakkeby/dwm-flexipatch) can be a good starting point so you can see how different patches work together.
  * [st](https://st.suckless.org/) ([my fork](https://codeberg.org/jos/st)) may not be *the best* **terminal emulator** (although it's very close), but once [scroll](https://tools.suckless.org/scroll/) works properly (hopefully soon but realistically never), it will be *the best* one for sure.
  * [slock](https://tools.suckless.org/slock/) ([my fork](https://codeberg.org/jos/slock)) is a nice looking **screen locker** that does what you expect it to. Spice it up with [picom](https://github.com/yshui/picom) for some elegant blurring.
  * [sxhkd](https://github.com/baskerville/sxhkd) to manage **keyboard shortcuts**. I personally like that it's WM agnostic and also provides chord chains. Very nice.
  * [gsimplecal](https://github.com/dmedvinsky/gsimplecal) shows a cute **calendar window**. I use it when clicking on the date section of the bar.
  * [mpv](https://mpv.io/) is one of the best **media players** that I've had the pleasure of using. I also use the [webm](https://github.com/ekisu/mpv-webm), [thumbfast](https://github.com/po5/thumbfast) and [quality-menu](https://github.com/christoph-heinrich/mpv-quality-menu) scripts.
  * [mpd](https://github.com/MusicPlayerDaemon/MPD) and [ncmpcpp](https://github.com/ncmpcpp/ncmpcpp) (with [this wrapper](https://github.com/tam-carre/ncmpcpp-ueberzug)) as the **music player**. Admittedly I haven't tried many music players (Cantata, Clementine and not much else), but I really like the sleek and clean look `ncmpcpp` offers.
  * [nsxiv](https://codeberg.org/nsxiv/nsxiv) as the **image viewer**. I'd say a different one, but I have yet to find a single image viewer that opens gif and animated webp as well as this one does. Very thankful for this piece of software to be honest, despite the odd defaults it has like not animating the images unless you wrap it or change the source.
  * [dmenu](https://tools.suckless.org/dmenu/) ([my fork](https://codeberg.org/jos/dmenu)) as the **program launcher**, and [rofi](https://github.com/davatorium/rofi) as a window switcher and power menu. The main reason I use dmenu as a launcher is because it's easier to configure (I won't say the number of times I've failed to make a nice looking rofi configuration from scratch).
  * [vifm](https://github.com/vifm/vifm) is a **file explorer** I keep always getting back to. Ever since I integrated [fzf](https://github.com/junegunn/fzf) with it, I can reach any folder or file living inside my homedir in a matter of seconds. Don't forget about [vifmimg](https://github.com/cirala/vifmimg/) if you want image previews!
  * [remind](https://dianne.skoll.ca/projects/remind/) is a nice **calendar program** that notifies you from events. The way i have it set-up will be explained on a later section.
  * [stalonetray](https://github.com/kolbusa/stalonetray) draws a **system tray area**. It has some quirks, and unfortunately the development seems stalled, but there aren't too many alternatives. One of them would be the systray patch for dwm, but it's quite big...

  ## SCRIPTS I CAN'T LIVE WITHOUT
  * [toggle.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/toggle.sh). I was a normal person before, I swear. That is, until I tried scratchpads... Toggle as many *named scratchpads* (windows that have a specific class, instance or title name) as you wish (or maybe as your keyboard allows you), or hide/unhide the window that has the current focus like if it was a *dynamic scratchpad*. I tried to combine the 2 most useful dwm patches on a single shell script that should be WM agnostic too! (*note that it could still be a bit experimental*)
  * [mpd-notification.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/mpd-notification.sh) and [mpdrand.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/mpdrand.sh). The first one shows a notification every time the state of an mpd song changes. The second one dynamically creates a random playlist when `mpd` is set to repeat: off, random: off and consume: on.
  * [extract](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/extract). Honestly I don't remember where I ~~stole~~ found this one, but it comes pretty handy, and is rather small too! Check out those dependencies for the file types you want to extract though.
</details>

<details>
  <summary>Dependencies</summary>

  **TLDR**: You want `xprop`, `xdo`, `xclip`, `icons-in-terminal`, `alsa-utils`, `pamixer` and `pulsemixer`.
  And depending on the software choices, you may want `xset`, `asciiquarium`, `ueberzug`, `mpc`, `light` and `xwallpaper`
  * `libXft` and `libXinerama` are only required if you start from a very very minimal install, since `st` and `dwm`, and probably `slock` and `dmenu` too need them to be compiled. Add `libnotify` as well for the sweet notifications. But in reality, pretty much any graphical program will have these as a dependency, so in most cases it won't be needed to manually install them.

  * `xset` (part of the Xorg suite of apps), can be safely skipped if you don't have any interest in screen locking after a set amount of inactivity time e.g. with the help of the [screensaver.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/screensaver.sh) script.
  * `xprop` is another component of the Xorg suite of apps, and is again required for [screensaver.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/screensaver.sh), but it is also required on [dwmbar](https://codeberg.org/jos/dotfiles/src/branch/master/wm/.config/dwm/dwmbar), only if you want to show a tray area with the help of `stalonetray`.
  * [asciiquarium](https://github.com/cmatsuoka/asciiquarium). The "famous" screen saver that will be launched from [screensaver.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/screensaver.sh). You should install it anyway, idk, maybe you will enjoy the mysteries of the sea from the safety of your own terminal too!
  * [xdo](https://github.com/baskerville/xdo) is required once again on [screensaver.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/screensaver.sh), on [toggle.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/toggle.sh), and last but not least on [shot.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/shot.sh). Instead, [xdotool](https://www.semicomplete.com/projects/xdotool/) could be used, but these 3 scripts will need to be adapted to `xdotool's` syntax.
  * [ImageMagick](https://github.com/ImageMagick/ImageMagick)'s import command is used inside [shot.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/shot.sh), but if you have installed any image manupulation program, chances are that this will be already installed.
  * [mpc](https://github.com/MusicPlayerDaemon/mpc) is a dependency of the mpd-related scripts [mpd-notification.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/mpd-notification.sh) and [mpdrand.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/mpdrand.sh), but it's also a neat utility to have around if `mpd` is being used, since it allows controlling `mpd` directly from the keyboard.
  * [ueberzug](https://github.com/seebye/ueberzug), needed for `ncmpcpp` cover art images and `vifm` image previews.
  * If *Arch Linux* is being used, [pacman-contrib](https://archlinux.org/packages/community/x86_64/pacman-contrib/) will help showing the available system updates on the [dwmbar](https://codeberg.org/jos/dotfiles/src/branch/master/wm/.config/dwm/dwmbar).
  * [imlib2](https://sourceforge.net/projects/enlightenment/), required by `nsxiv` as a hard dependency, and `libexif`, also required by `nsxiv` but as an optional dependency
  * [xclip](https://github.com/astrand/xclip) allows copying pretty much anything to the clipboard. It's required by [shot.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/shot.sh), but it's also very useful and you would want it regardless.
  * [light](https://github.com/haikarainen/light) controls backlight levels and is used on [backlight.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/backlight.sh). As an alterntive, `xbacklight` can be used, but the script will need to be tweaked.
  * [inotify-tools](https://github.com/inotify-tools/inotify-tools), used on the battery and scratchpad modules from [dwmbar](https://codeberg.org/jos/dotfiles/src/branch/master/wm/.config/dwm/dwmbar).
  * [xwallpaper](https://github.com/stoeckmann/xwallpaper). Dependency of [setbg](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/setbg) to be able to set the wallpaper. Both the utility and the script could be replaced by `feh` or similar if you'd like, but they are quite minimal and do what you expect them to.
  * [pamixer](https://github.com/cdemoulins/pamixer) is a (quite fast) cli mixer that is needed by [volume.sh](https://codeberg.org/jos/dotfiles/src/branch/master/scripts/.local/bin/volume.sh), and [pulsemixer](https://github.com/GeorgeFilipkin/pulsemixer) is a curses mixer that is very nice to have around.
  * [alsa-utils](https://github.com/alsa-project/alsa-utils). Particularly, the `amixer` utility is the responsible of showing instantaneous volume changes on the bar.
  * [icons-in-terminal](https://github.com/sebastiencs/icons-in-terminal) is my choice of an icon font that is used on the `dwmbar` and `vifm`. Other font pack could be used, but all the icons will need to be redefined.
</details>

<details>
  <summary>Post-install steps and quirks</summary>

  * First and foremost: reboot. Allow all the files to be sourced properly.

  * Run `vim` once. This should install [vim-plug](https://github.com/junegunn/vim-plug) and all of the different vim plugins automagically.
  * Add the directories you want excluded from fzf's search to ~/.config/find/ignoredirs, if any. (Create the file or run `$ ignored-dirs` once to create a template).
  * If `mpd` was installed, it may be desired to change the music directory path (it defaults to ~/Music), both inside [mpd.conf](https://codeberg.org/jos/dotfiles/src/branch/master/music/.config/mpd/mpd.conf) and [ncmpcpp_cover_art.sh](https://codeberg.org/jos/dotfiles/src/branch/master/music/.config/ncmpcpp-ueberzug/ncmpcpp_cover_art.sh). Also, if one dislikes experimenting with pipewire, and prefers the safety pulseaudio offers, the `mpd.conf` audio output section should be changed too.
  * Now, take a look inside [.profile](https://codeberg.org/jos/dotfiles/src/branch/master/shell/.profile) and change any variable you don't like. I'm talking particularly about `$VISUAL`, `$EDITOR`, `$TERMINAL` and `$BROWSER`.
  * Talking about, a lot of the environment variables inside `.profile` help having a clean home, but all of this comes with a cost: `GnuPg` will complain that the directory doesn't exist if you try to generate a key pair, so you must create it manually (only once thank god); `$ mkdir -p "$GNUPGHOME" && chmod 700 "$GNUPGHOME"`. ~~I should really need to find a way to automate this...~~
  * About `remind`. I have two main files: `reminders.rem` and `timed.rem`. These files live inside the directory provided by the `$REMIND_DIR` variable. The file `reminders.rem` will simply source every other file inside the directory. So let's say we have these two files, plus a `birthdays.rem` file that contains a bunch of birthdays. The contents inside `reminders.rem` will consist of 2 lines like the following: `include path/to/birthdays.rem` and `include path/to/timed.rem` (note that the `/home/user` part can be safely skipped). The first file, `reminders.rem`, will be monitored by `dwmbar`, which will display the total number of tasks issued for today, and clicking on that number will show a notification with the actual tasks. On the other hand, the file `timed.rem` will be monitored by remind in daemon mode, and will only contain reminders that trigger at a specific hour with the `AT` keyword. When the time has come for the remind to trigger, a notification with the reminder will be automatically shown. I've included [sample files](https://codeberg.org/jos/dotfiles/src/branch/master/misc/remind/.local/share/remind) in the repository to help you see how it works.
  * Last but not least, `$ setbg /path/to/image` will set the wallpaper, and it will be reapplied after every reboot because `setbg` will be called inside the autostart file.

</details>

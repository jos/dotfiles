#!/usr/bin/env sh

# https://zihao.me/post/managing-dotfiles-with-gnu-stow/
# The script will try to install the packages to their respective location,
# backing up all the files that would produce conflicts

PACKAGES="dunst git mpv music nsxiv picom qutebrowser rofi scripts shell vifm vim wm x11 zathura"

if ! [ -x "$(command -v stow)" ]; then
    echo "You should install GNU Stow first!"
    echo "https://www.gnu.org/software/stow/"
    exit 1
fi

for PKG in $PACKAGES; do
    CONFLICTS=$(stow --no --verbose $PKG 2>&1 | awk '/\* existing target is/ {print $NF}')
    for filename in ${CONFLICTS}; do
        if [ -f $HOME/$filename ] || [ -L $HOME/$filename ]; then
            echo "BACKING UP: $filename"
            mv "$HOME/$filename" "$HOME/$filename.bak"
        fi
    done

    # handle git-hooks separately, but only if git is enabled in the PACKAGES array
    if [ "$PKG" = "git" ]; then
        mkdir -pv "${XDG_CONFIG_HOME:-$HOME/.config}/git"
        cp -nrv git-hooks/git_template/ "${XDG_CONFIG_HOME:-$HOME/.config}/git"
    fi

    stow --no-folding --verbose "$PKG"
done

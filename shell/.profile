#
# ~/.profile
#

# Make ~/.local/bin folder executable
export PATH="$PATH:$HOME/.local/bin"

# XDG Base Directory
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# Default programs
export EDITOR='vim'
export VISUAL='vim'
export TERMINAL='st'
export BROWSER='qutebrowser'

# FZF
export FZF_DEFAULT_OPTS="--bind change:first --inline-info"
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200'"
export FZF_DEFAULT_COMMAND='find "$HOME" \! \( \( $(ignored-dirs) \) -prune \)'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="$FZF_DEFAULT_COMMAND -type d"

# Fcitx
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx

# ninfs
export BOOT9_PATH="${XDG_DATA_HOME:-$HOME/.local/share}/3ds/boot9.bin"
export SEEDDB_PATH="${XDG_DATA_HOME:-$HOME/.local/share}/3ds/seeddb.bin"

# pfetch
export PF_INFO="ascii title os kernel shell pkgs wm memory"
export PF_SEP=":"

# Other
export GNUPGHOME="${XDG_DATA_HOME:-$HOME/.local/share}/gnupg"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc"
export HISTFILE="${XDG_STATE_HOME:-$HOME/.local/state}/history"
export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/readline/inputrc"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="${XDG_CONFIG_HOME:-$HOME/.config}/java"
export LESSHISTFILE="-"
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/pass"
export QT_QPA_PLATFORMTHEME=qt5ct
export REMIND_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/remind"
export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wine"
export XINITRC="${XDG_CONFIG_HOME:-$HOME/.config}/X11/xinitrc"
export XAUTHORITY="${XDG_RUNTIME_DIR:-/run/user/"$(id -u)"}/Xauthority" # Remove if using a DM

# Fix misbehaving Java applications on dwm
export _JAVA_AWT_WM_NONREPARENTING=1

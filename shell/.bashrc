#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PROMPT_COMMAND=__prompt_command

__prompt_command() {
    PS1=""

    # vifm instance
    [ -n "$INSIDE_VIFM" ] && PS1+="\[\e[0;35m\][$INSIDE_VIFM] \[\e[0m\]"

    # exit code
    PS1+="\[\e[0;31m\]\${?#0}\[\e[0m\]"

    # user, host and current directory
    PS1+='\[\e[1;36m\]\u\[\e[1;36m\]@\[\e[1;36m\]\h\[\e[1;37m\]: \[\e[1;33m\]\w'

    # git branch
    [ -r "$GIT_PROMPT" ] && PS1+="\[\e[1;32m\] $(__git_ps1 "( %s)")"

    # rest of the prompt
    PS1+="\[\e[0m\]\n$ "

    # If this is an xterm set the titlebar to user@host: dir
    case "$TERM" in
    xterm*|rxvt*|st*)
        PS1="\[\e]2;\u@\h: \w\a\]$PS1"
        ;;
    *)
        ;;
    esac

    # new line before every prompt except the first time
    prompt_newline
}

prompt_newline() {
    prompt_newline() {
        echo
    }
}

# vi mode in bash
set -o vi
bind 'set show-mode-in-prompt on'
bind 'set vi-ins-mode-string +'
bind 'set vi-cmd-mode-string -'

# completion
bind 'TAB:menu-complete'
bind 'set show-all-if-ambiguous on'

# bash history
HISTSIZE=10000
HISTFILESIZE=10000
HISTCONTROL=ignoredups:erasedups
shopt -s histappend

# Disable ctrl-s and ctrl-q
stty -ixon

# fzf
FZF_BINDINGS="/usr/share/fzf/key-bindings.bash"
FZF_COMPLETION="/usr/share/fzf/completion.bash"
[ -r "$FZF_BINDINGS" ] && . "$FZF_BINDINGS"
[ -r "$FZF_COMPLETION" ] && . "$FZF_COMPLETION"

# git prompt
GIT_PROMPT="/usr/share/git/completion/git-prompt.sh"
[ -r "$GIT_PROMPT" ] && . "$GIT_PROMPT"

# shell functions
SHELL_FUNCTIONS="${XDG_DATA_HOME:-$HOME/.local/share}/shell_functions"
[ -r "$SHELL_FUNCTIONS" ] && . "$SHELL_FUNCTIONS"

# aliases
alias bc='bc -q -l'
alias cp='cp -iv'
alias diff='diff --color=auto'
alias ffmpeg='ffmpeg -hide_banner'
alias grep='grep -i --color=auto'
alias ls='ls -hN --color=auto --group-directories-first'
alias mkdir='mkdir -pv'
alias mv='mv -iv'
alias pcache='paru -Sc'
alias please='sudo' # polite bash
alias porphan='paru -Rns $(paru -Qtdq)'
alias rm='rm -Iv'
alias vifm='vifmrun'
alias weather='curl wttr.in'
alias wget="wget --hsts-file ${XDG_DATA_HOME:-$HOME/.local/share}/wget-hsts"
alias yay='paru'


# (mis)using the DEBUG signal to change the terminal title to the current command
trap 'echo -ne "\033]2;$(history 1 | sed "s/^[ ]*[0-9]*[ ]*//g")\007"' DEBUG

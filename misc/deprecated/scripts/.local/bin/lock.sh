#!/bin/sh
# Requires this fork of i3lock (https://github.com/Raymo111/i3lock-color)
# config for i3lock-color, based on betterlockscreen

i3lock \
    --time-pos='x+110:h-70' \
    --date-pos='x+43:h-45' \
    --clock --date-align 1 --date-str="Type password to unlock..." \
    --color=00000055 --inside-color=00000000 --ring-color=ffffffff \
    --line-uses-inside --keyhl-color=d23c3dff --bshl-color=d23c3dff \
    --separator-color=00000000 --insidever-color=00000000 \
    --insidewrong-color=d23c3dff --ringver-color=ffffffff \
    --ringwrong-color=ffffffff --ind-pos='x+280:h-70' \
    --radius=20 --ring-width=4 --verif-text='' --wrong-text='' \
    --verif-color=ffffffff --time-color=ffffffff --date-color=ffffffff \
    --time-font=sans-serif --date-font=sans-serif \
    --layout-font=sans-serif --verif-font=sans-serif \
    --wrong-font=sans-serif \
    --noinput-text='' --force-clock --pass-media-keys \
    --ignore-empty-password

#!/bin/sh

# this script shows a notification with the tasks for today,
# or prints the total number of tasks, if called with -l
while getopts "ln" opt; do
    case ${opt} in
        n)
            dunstify -t 5000 -i /usr/share/icons/Papirus-Dark/24x24/actions/bell.svg "$(eval "remind -ga $HOME/.config/remind/reminders.rem | awk '/\\w/ {print}'")"
            ;;
        l)
            eval "remind -ga $HOME/.config/remind/reminders.rem | awk '/\\w/ {if (NR!=1) {print}}'" | wc -l
            ;;
        *)
            printf "%s\n" "Invalid option: $OPTARG" 1>&2
            ;;
    esac
done

#!/bin/sh

# Toggle a ncmpcpp instance with a custom set of bspwm rules
id=$(xdo id -N mpdplayer)

if [ "$id" != "" ]; then
    for a in $id; do
        xdo close "$a"
    done
else
    st -c mpdplayer -e 'ncmpcpp-ueberzug' >/dev/null 2>&1 &
fi

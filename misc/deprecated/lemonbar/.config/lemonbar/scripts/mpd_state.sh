#!/bin/sh

# returns the current mpd state, you can get most of the
# functionality through 'mpc status', but afaik it doesn't
# show if mpd is stopped.
# This script lets me add a custom string to lemonbar if
# there's no song playing, otherwise it would show
# an empty string
if [ "$(mpc status | grep playing)" ]; then
    printf "%s\n" "playing"
elif [ "$(mpc status | grep paused)" ]; then
    printf "%s\n" "paused"
else
    printf "%s\n" "stopped"
fi

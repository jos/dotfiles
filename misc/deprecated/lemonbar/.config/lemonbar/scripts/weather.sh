#!/bin/sh

# Icons (shuld be compatible with POSIX shells like dash)
ERROR="\356\245\251"                # \ue969
CLOUDY="\356\244\207"               # \ue907
CLOUDY_FOG="\356\244\210"           # \ue908
BLIZZARD="\356\245\223"             # \ue953
PELLETS_DAY="\356\243\271"          # \ue8f9
PELLETS_NIGHT="\356\244\226"        # \ue916
THUNDER_DAY="\356\243\272"          # \ue8fa
THUNDER_NIGHT="\356\244\227"        # \ue917
LIGHT_RAIN_DAY="\356\243\273"       # \ue8fb
LIGHT_RAIN_NIGHT="\356\244\230"     # \ue918
HEAVY_RAIN_DAY="\356\243\274"       # \ue8fc
HEAVY_RAIN_NIGHT="\356\244\231"     # \ue919
RAIN_DAY="\356\243\275"             # \ue8fd
RAIN_NIGHT="\356\244\232"           # \ue91a
SHOWERS_DAY="\356\243\276"          # \ue8fe
SHOWERS_NIGHT="\356\244\233"        # \ue91b
SNOW_DAY="\356\243\277"             # \ue8ff
SNOW_NIGHT="\356\244\234"           # \ue91c
THUNDERSTORM_DAY="\356\244\204"     # \ue904
THUNDERSTORM_NIGHT="\356\244\237"   # \ue91f
CLEAR_DAY="\356\244\202"            # \ue902
CLEAR_NIGHT="\356\244\240"          # \ue920
FOG_DAY="\356\243\270"              # \ue8f8
FOG_NIGHT="\356\244\273"            # \ue93b
HEAVY_SNOW_DAY="\356\245\224"       # \ue954
HEAVY_SNOW_NIGHT="\356\245\226"     # \ue956
THUNDER_SNOW_DAY="\356\245\232"     # \ue95a
THUNDER_SNOW_NIGHT="\356\245\234"   # \ue95c
CLOUDY_DAY="\356\243\267"           # \ue8f7
CLOUDY_NIGHT="\356\245\263"         # \ue973
SLEET_DAY="\356\246\237"            # \ue99f
SLEET_NIGHT="\356\246\241"          # \ue9a1


weather=$(curl -s 'wttr.in/?format=%t')

if [ $? -eq 0 ]; then

    # Check if wttr.in is down but still reports Unknown location
    if test "${weather#*'Unknown location'}" != "$weather"; then
        printf "%s\n" "WTR"
        exit 1
    fi

    hour=$(date +%H)
    hour_decimal=${hour#0}
    weather_condition=$(curl -s 'wttr.in/?format=%C' )

    if [ "$hour_decimal" -ge "20" ] || [ "$hour_decimal" -le "7" ]; then
        case $weather_condition in
            'Overcast') icon=$CLOUDY;;
            'Freezing fog') icon=$CLOUDY_FOG;;
            'Ice pellets') icon=$PELLETS_NIGHT;;
            'Thundery outbreaks possible') icon=$THUNDER_NIGHT;;
            *[Ll]ight*rain) icon=$LIGHT_RAIN_NIGHT;;
            'Patchy rain possible') icon=$LIGHT_RAIN_NIGHT;;
            *[Hh]eavy*rain*) icon=$HEAVY_RAIN_NIGHT;;
            'Moderate or heavy sleet showers') icon=$HEAVY_RAIN_NIGHT;;
            'Torrential rain shower') icon=$HEAVY_RAIN_NIGHT;;
            Moderate*rain*) icon=$RAIN_NIGHT;;
            Rain) icon=$RAIN_NIGHT;;
            *[Dd]rizzle*) icon=$SHOWERS_NIGHT;;
            Light\ rain*) icon=$SHOWERS_NIGHT;;
            'Light sleet showers') icon=$SHOWERS_NIGHT;;
            Light\ snow*) icon=$SNOW_NIGHT;;
            'Patchy light snow') icon=$SNOW_NIGHT;;
            'Moderate snow') icon=$SNOW_NIGHT;;
            'Patchy moderate snow') icon=$SNOW_NIGHT;;
            *rain\ with\ thunder) icon=$THUNDERSTORM_NIGHT;;
            *[Tt]hunderstorm*) icon=$THUNDERSTORM_NIGHT;;
            Clear|Sunny) icon=$CLEAR_NIGHT;;
            Fog|*[Mm]ist*) icon=$FOG_NIGHT;;
            'Shallow fog') icon=$FOG_NIGHT;;
            Blizzard) icon=$BLIZZARD;;
            'Blowing snow') icon=$HEAVY_SNOW_NIGHT;;
            *[Hh]eavy\ snow) icon=$HEAVY_SNOW_NIGHT;;
            'Moderate or heavy snow showers') icon=$HEAVY_SNOW_NIGHT;;
            *snow\ with\ thunder) icon=$THUNDER_SNOW_NIGHT;;
            *[Cc]loudy) icon=$CLOUDY_NIGHT;;
            'Patchy snow possible') icon=$SLEET_NIGHT;;
            'Patchy sleet possible') icon=$SLEET_NIGHT;;
            *sleet) icon=$SLEET_NIGHT;;
            *) icon=$ERROR;;
        esac
    else
        case $weather_condition in
            *[Cc]loudy) icon=$CLOUDY_DAY;;
            Fog|*[Mm]ist*) icon=$FOG_DAY;;
            'Shallow fog') icon=$FOG_DAY;;
            'Ice pellets') icon=$PELLETS_DAY;;
            'Thundery outbreaks possible') icon=$THUNDER_DAY;;
            *[Ll]ight*rain) icon=$LIGHT_RAIN_DAY;;
            'Patchy rain possible') icon=$LIGHT_RAIN_DAY;;
            *[Hh]eavy*rain*) icon=$HEAVY_RAIN_DAY;; #rain wind
            'Moderate or heavy sleet showers') icon=$HEAVY_RAIN_DAY;;
            'Torrential rain shower') icon=$HEAVY_RAIN_DAY;;
            Moderate*rain*) icon=$RAIN_DAY;;
            Rain) icon=$RAIN_DAY;;
            *[Dd]rizzle*) icon=$SHOWERS_DAY;;
            Light\ rain*) icon=$SHOWERS_DAY;;
            'Light sleet showers') icon=$SHOWERS_DAY;;
            Light\ snow*) icon=$SNOW_DAY;;
            'Patchy light snow') icon=$SNOW_DAY;;
            'Moderate snow') icon=$SNOW_DAY;;
            'Patchy moderate snow') icon=$SNOW_DAY;;
            Clear|Sunny) icon=$CLEAR_DAY;;
            *rain\ with\ thunder) icon=$THUNDERSTORM_DAY;;
            *[Tt]hunderstorm*) icon=$THUNDERSTORM_DAY;;
            'Overcast') icon=$CLOUDY;;
            'Freezing fog') icon=$CLOUDY_FOG;;
            'Blizzard') icon=$BLIZZARD;;
            'Blowing snow') icon=$HEAVY_SNOW_DAY;;
            *[Hh]eavy\ snow) icon=$HEAVY_SNOW_DAY;;
            'Moderate or heavy snow showers') icon=$HEAVY_SNOW_DAY;;
            *snow\ with\ thunder) icon=$THUNDER_SNOW_DAY;;
            'Patchy snow possible') icon=$SLEET_DAY;;
            'Patchy sleet possible') icon=$SLEET_DAY;;
            *sleet) icon=$SLEET_DAY;;
            *) icon=$ERROR;;
        esac
    fi
    printf "%s\n" "WTR$icon $weather  "
else
    printf "%s\n" "WTR"
fi

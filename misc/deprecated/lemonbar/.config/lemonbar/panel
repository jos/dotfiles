#! /bin/sh

. $HOME/.config/lemonbar/icons

PANEL_FIFO="/tmp/panel.fifo"
LEMON_SCRIPTS="$HOME/.config/lemonbar/scripts"
PANEL_HEIGHT=25
PANEL_FONT1="monospace:style=Regular:pixelsize=12:antialias=true"
PANEL_FONT2="IPAGothic:antialias=true:pixelsize=14:antialias=true"
PANEL_FONT3="icons\-in\-terminal:pixelsize=14"
PANEL_WM_NAME="bspwm_panel"

# dracula
COLOR_DEFAULT_FG="#f8f8f2"
COLOR_DEFAULT_BG="#282a36"

if xdo id -a "$PANEL_WM_NAME" > /dev/null ; then
	printf "%s\n" "The panel is already running." >&2
	exit 1
fi

trap 'trap - TERM; kill 0' INT TERM QUIT EXIT

[ -e "$PANEL_FIFO" ] || mkfifo "$PANEL_FIFO"


# bspwm
bspc subscribe report > "$PANEL_FIFO" &


# mpd song (scrolling with zscroll)
# Actions: left click to open ncmpcpp in a floating window, and seek using the wheel
while :; do
    if ! mpc >/dev/null 2>&1; then
        printf "%s\n" "MPDServer Offline"
    else
        zscroll --before-text "MPD%{A:$LEMON_SCRIPTS/mpdplayer.sh:}%{A4:mpc seek -1 >/dev/null 2>&1:}%{A5:mpc seek +1 >/dev/null 2>&1:}" \
                --after-text "%{A}%{A}%{A}" --delay 0.3 \
                --length 25 --scroll-padding "  ~  " \
                --match-text "playing" "--scroll 1" \
                --match-text "paused" "--scroll 0" \
                --match-text "stopped" \
                "--after-text 'Stopped Playback%{A}%{A}%{A}'" \
		--match-command "$LEMON_SCRIPTS/mpd_state.sh" \
                --update-check true "mpc current"
    fi
    sleep 5 # Sleep in case something fails or is not installed
done > "$PANEL_FIFO" &


# mpc playback commands
while :; do
    if ! mpc >/dev/null 2>&1; then

        # we kill zscroll from here in case of failure because the
        # main module will be busy listening for song updates
        pgrep -x zscroll && pkill zscroll
        printf "%s\n" "MPC"
        sleep 5
    else
        if mpc status | grep -q playing; then
            toggle=$PAUSE
        else
            toggle=$PLAY
        fi

        # for some reason that I can't figure out, actions are offseted one character to the right for this module
        # the weird icon placement is a workaround for that
        printf "%b\n" "MPC%{A:mpc prev >/dev/null:}  %{A}%{A:mpc toggle >/dev/null:}$PREV %{A}%{A:mpc next >/dev/null:}${toggle} %{A}$NEXT"

        if command -v mpc >/dev/null; then
            mpc idle >/dev/null
        else
            sleep 60
        fi
    fi
done > "$PANEL_FIFO" &


# toggle mpd repeat_single, random and consume modes
while :; do
    if ! mpc >/dev/null 2>&1; then
        printf "%s\n" "RZS"
        sleep 5
    else
        repeatsingle=$($LEMON_SCRIPTS/mpd_mode_helper.sh -r)
        randomicon=$($LEMON_SCRIPTS/mpd_mode_helper.sh -z)
        consumeicon=$($LEMON_SCRIPTS/mpd_mode_helper.sh -c)

        printf "%b\n" "RZS%{A:mpc random >/dev/null:}$randomicon %{A}%{A:$LEMON_SCRIPTS/mpd_mode_helper.sh -R:}$repeatsingle %{A}%{A:mpc consume >/dev/null:}$consumeicon%{A}"

        if command -v mpc >/dev/null; then
            mpc idle >/dev/null
        else
            sleep 60
        fi
    fi
done > "$PANEL_FIFO" &


# date
while :; do
    printf "%s\n" "DAT%{A:gsimplecal:}$(date +'%a %d %b, %T')%{A}"
    sleep 1
done > "$PANEL_FIFO" &


# pulseaudio volume
first=true
pactl subscribe | grep --line-buffered "sink" | while $first || read line; do
    first=false
    PULSE_VOLUME=$(pamixer --get-volume)
    PULSE_VOLUME_P="${PULSE_VOLUME}%%"
    MUTED=$(pamixer --get-mute)

    if [ "$MUTED" = "true" ]; then
        ICON=$MUTE
    elif [ "$PULSE_VOLUME" -lt 25 ]; then
        ICON=$VOLUME_LOW
    elif [ "$PULSE_VOLUME" -lt 50 ]; then
        ICON=$VOLUME_MID
    else
        ICON=$VOLUME_HIGH
    fi

    printf "%b\n" "VOL%{A1:volume.sh -m:}%{A4:volume.sh -i 1:}%{A5:volume.sh -d 1:}$ICON $PULSE_VOLUME_P%{A}%{A}%{A}"
done > "$PANEL_FIFO" &


# battery
while :; do

    # With the help of an udev rule (look in the 'udev' directory), update when plugged/unplugged
    # Check the capacity every 60 seconds otherwise
    [ -e /tmp/batt_status ] || touch /tmp/batt_status

    BATC=$(cat /sys/class/power_supply/BAT0/capacity)
    BATS=$(cat /sys/class/power_supply/BAT0/status)

    if [ "$BATS" = "Charging" ]; then
        ICON=$BATT_CHARGING
    elif [ "$BATC" -gt 20 ]; then
        ICON=$BATT_HIGH
    else
        ICON=$BATT_LOW
    fi

    printf "%b\n" "BAT${ICON} ${BATC}%"

    if command -v inotifywait >/dev/null; then
        inotifywait -qt 60 /tmp/batt_status
    else
        sleep 60
    fi
done > "$PANEL_FIFO" &


# updates
while :; do
    sleep 90 # Hopefully we are online after 90 seconds
    PAC=$(checkupdates 2> /dev/null | wc -l)
    AUR=$(paru -Qum 2> /dev/null | wc -l)
    UPDATES=$(( PAC + AUR ))

    if [ "$UPDATES" -eq 0 ]; then
        printf "%s\n" "UPD"
    else
        printf "%b\n" "UPD%{A:$LEMON_SCRIPTS/pkg.sh &:}$PKG_ARCH ${UPDATES}%{A}  "
    fi
    sleep 1110
done > "$PANEL_FIFO" &


# CPU usage (only show it if it's above 70%)
while :; do
    CPU=$(top -b -n1 | grep ^%Cpu | awk '{printf("%.2f"), 100-$8}')
    if awk 'BEGIN{exit ARGV[1]<ARGV[2]}' "$CPU" "70"; then
        printf "%b\n" "CPU$CPU_ICON ${CPU}%%  "
    else
        printf "%s\n" "CPU"
    fi
    sleep 10
done > "$PANEL_FIFO" &


# RAM usage (only show it if it's above 60%)
while :; do
    MEM=$(free -t | awk 'NR == 2 {printf("%.2f"), $3/$2*100}')
    if awk 'BEGIN{exit ARGV[1]<ARGV[2]}' "$MEM" "60"; then
        printf "%b\n" "MEM$RAM_ICON ${MEM}%%  "
    else
        printf "%s\n" "MEM"
    fi
    sleep 10
done > "$PANEL_FIFO" &


# bluetooth
rfkill event | while read -r line; do

    # Returns 0 if bluetooth is enabled
    BTICON=$(rfkill list bluetooth | grep -c "yes")
    if [ "$BTICON" -eq 0 ]; then
        icon=$BT_ON
        BTSTATUS="on "
    else
        icon=$BT_OFF
        BTSTATUS="off"
    fi
    printf "%b\n" "BLT%{A:rfkill toggle bluetooth:}%{A2:blueberry >/dev/null 2>&1:}$icon ${BTSTATUS}%{A}%{A}"
done > "$PANEL_FIFO" &


# power button
printf "%b\n" "PWR%{A:$HOME/.config/rofi/scripts/powermenu.sh >/dev/null 2>&1:}$POWER_ICON %{A}" > "$PANEL_FIFO" &


# remind
while :; do
    if [ "$(remind $HOME/.config/remind/reminders.rem | awk '{print $1}')" = "No" ]; then
        printf "%s\n" "REM"
    else
        NUMBER_TASKS=$($LEMON_SCRIPTS/reminders.sh -l)
        printf "%b\n" "REM%{A:$LEMON_SCRIPTS/reminders.sh -n:}$BELL_ICON $NUMBER_TASKS  %{A}"
    fi
    sleep 3600
done > "$PANEL_FIFO" &


# weather
while :; do
    sleep 90 # Hopefully we are online after 90 seconds
    WEATHER=$($LEMON_SCRIPTS/weather.sh)
    printf "%b\n" "$WEATHER"
    sleep 1710
done > "$PANEL_FIFO" &


# system tray
while :; do
    # ask xprop about the window width, then draw 3 spaces per slot (the slot size is 20 per stalonetrayrc)
    # if stalonetray is restarted, killed, etc. Wait 3 seconds before trying to enter the main loop again
    if pgrep -x stalonetray; then
        xprop -name stalonetray -f WM_SIZE_HINTS 32i ' $5\n' -spy WM_NORMAL_HINTS | while read -r line; do
            slot_size=20
            width=$(xprop -name stalonetray -f WM_SIZE_HINTS 32i ' $5\n' WM_NORMAL_HINTS | sed 's/.* //g')
            traybar=$(printf "%*s" "$(( ($width/$slot_size)*3 ))")
            printf "%s\n" "TRAY${traybar}"
        done
    else
        printf "%s\n" "TRAY"
        sleep 3
    fi
done > "$PANEL_FIFO" &


~/.config/lemonbar/panel_bar < "$PANEL_FIFO" | lemonbar -a 32 -u 2 -n "$PANEL_WM_NAME" -g x$PANEL_HEIGHT -o 1 -f "$PANEL_FONT1" -o 1 -f "$PANEL_FONT2" -o 1 -f "$PANEL_FONT3" -F "$COLOR_DEFAULT_FG" -B "$COLOR_DEFAULT_BG" | sh &

wid=$(xdo id -m -a "$PANEL_WM_NAME")
xdo above -t "$(xdo id -N Bspwm -n root | sort | head -n 1)" "$wid"

wait

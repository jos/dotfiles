#!/bin/sh

# modified script to browse through images in directory after opening
# a single file in imv
# original script for sxiv here:
# https://github.com/ranger/ranger/blob/master/examples/rifle_sxiv.sh

if [ $# -eq 0 ]; then
    echo "Usage: ${0##*/} PICTURES"
    exit
fi

[ "$1" = '--' ] && shift

abspath () {
    case "$1" in
        /*) printf "%s\n" "$1";;
        *)  printf "%s\n" "$PWD/$1";;
    esac
}

listfiles () {
    find -L "$(dirname "$target")" -maxdepth 1 -type f -iregex \
      '.*\(jpe?g\|bmp\|png\|gif\|webp\|avif\)$' -print0 | sort -z
}

target="$(abspath "$1")"
count="$(listfiles | grep -m 1 -ZznF "$target" | cut -d: -f1)"

if [ -n "$count" ]; then
    listfiles | xargs -0 imv -n "$count" --
else
    imv -- "$@" # fallback
fi

# ======================
# wm independent hotkeys
# ======================

# terminal emulator
super + Return
        st

# scratchpad
super + BackSpace
        toggle.sh st -c scratchterm

# application launcher
super + z
        $HOME/.config/rofi/scripts/appsmenu.sh

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd

# =============
# bspwm hotkeys
# =============

# quit/restart bspwm
super + alt + {q,r}
	bspc {quit 1 && pkill panel && pkill lemonbar,wm -r}

# close and kill
super + {_,shift + }q
	bspc node -{c,k}

# alternate between the tiled and monocle layout
super + m
	bspc desktop -l next

# send the newest marked node to the newest preselected node
super + y
	bspc node newest.marked.local -n newest.!automatic.local

# swap the current node and the biggest node
super + g
	bspc node -s biggest

# Move current window to a pre-selected space
super + shift + m
        bspc node -n last.!automatic

# Change window gap
super + equal
  bspc config window_gap $(expr $(bspc config window_gap) - 2)

super + minus
  bspc config window_gap $(expr $(bspc config window_gap) + 2)

# ===========
# state/flags
# ===========

# set the window state
super + {t,shift + t,s,f}
	bspc node -t {tiled,pseudo_tiled,floating,fullscreen} || bspc node -t tiled

# set the node flags
super + ctrl + {m,x,y,z}
	bspc node -g {marked,locked,sticky,private}

# ==========
# focus/swap
# ==========
 
# focus the node in the given direction
#super + {_,shift + }{h,j,k,l}
#        LAYOUT=$(bspc query -T -d | jq -r .layout); \
#        [[ "$LAYOUT" == "tiled" ]] && bspc node -{f,s} {west,south,north,east}.local; \
#        [[ "$LAYOUT" == "monocle" ]] && bspc node -{f,s} {prev,prev,next,next}.local
super + {_,shift + }{h,j}
        LAYOUT=$(bspc query -T -d | jq -r .layout); \
        [[ "$LAYOUT" == "tiled" ]] && bspc node -{f,s} {west,south}.local; \
        [[ "$LAYOUT" == "monocle" ]] && bspc node -{f,s} prev.local.!hidden.window
super + {_,shift + }{k,l}
        LAYOUT=$(bspc query -T -d | jq -r .layout); \
        [[ "$LAYOUT" == "tiled" ]] && bspc node -{f,s} {north,east}.local; \
        [[ "$LAYOUT" == "monocle" ]] && bspc node -{f,s} next.local.!hidden.window

# focus the node for the given path jump
#super + {p,b,comma,period}
#	bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
super + {_,shift + }c
	bspc node -f {next,prev}.local.!hidden.window

# focus the next/previous desktop in the current monitor
super + bracket{left,right}
	bspc desktop -f {prev,next}.local

# focus the last node/desktop
super + {grave,Tab}
	bspc {node,desktop} -f last

# focus the older or newer node in the focus history
super + {o,i}
	bspc wm -h off; \
	bspc node {older,newer} -f; \
	bspc wm -h on

# focus the given desktop
super + {1-9,0}
	bspc desktop -f '^{1-9,10}'

# Send to desktop
super + shift + {1-9,0}
        bspc node -d '^{1-9,10}' --follow

# Send to monitor
super + shift + equal
        bspc node -m last --follow

# =========
# preselect
# =========

# preselect the direction
super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
	bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

# ===========
# move/resize
# ===========

# resize windows
super + alt + {h,j,k,l}
    {bspc node @parent/second -z left -20 0; \
    bspc node @parent/first -z right -20 0, \
    bspc node @parent/second -z top 0 +20; \
    bspc node @parent/first -z bottom 0 +20, \
    bspc node @parent/first -z bottom 0 -20; \
    bspc node @parent/second -z top 0 -20, \
    bspc node @parent/first -z right +20 0; \
    bspc node @parent/second -z left +20 0}

# move a floating window
super + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}

# ==========
# extra keys
# ==========

# Volume up/down
XF86Audio{Raise,Lower}Volume + {_,shift}
        volume.sh {-i,-d} {3,1}

# Mute audio
XF86AudioMute
        volume.sh -m

# Brightness up/down
XF86MonBrightness{Up,Down} + {_,shift}
        backlight.sh {-i,-d} {5,1}

# Mpc control
super + {comma,period,slash,Delete}
        mpc {prev,toggle,next,clear}

super + shift + {comma,period,slash}
        mpc seek {-5,0%,+5}

super + control + {comma,slash}
        mpc {random,consume}

super + control + period
        $HOME/.config/lemonbar/scripts/mpd_mode_helper.sh -R

# Lock screen
control + alt + l
        lock.sh

# Rotate layout
super + {_,shift +} r
        bspc node @/ -R {90,270}

# print screen
Print + {_,shift,control}
        printscreen {-d,-s,-w}

super + Print + {_,shift,control}
        printscreen {-D,-S,-W}

# =========
# Shortcuts
# =========

super + control + q
        $HOME/.config/rofi/scripts/powermenu.sh
        #$HOME/.scripts/rofi-widgets/widgets/power-options.sh

alt + Tab
        rofi -config config-sleek.rasi -theme themes/middle-list.rasi -show window -kb-accept-entry '!Alt-Tab,!Alt+Alt_L' -kb-row-down Alt-Tab

super + p
        firefox --private-window

super + x
        qutebrowser

super + shift + x
        qutebrowser --nowindow ':open -p'

super + space; v
        st -e 'vifmrun'

super + space; t
        telegram-desktop

super + space; m
        toggle.sh st -c mpdplayer -e 'ncmpcpp-ueberzug'

super + space; p
        st -e 'pulsemixer'

super + space; b
        blueberry

super + v
        codium

# grab the selected color in rgb format and store it in the clipboard
# uses Jack12816/colorpicker
super + space; c
        colorpicker --short --one-shot --preview | tr -d '\n' | xclip -selection clipboard

# ===============
# Dunst Shortcuts
# ===============
alt + q
        dunstctl close

ctrl + alt + q
        dunstctl close-all

alt + grave
        dunstctl history-pop

alt + Return
        dunstctl action

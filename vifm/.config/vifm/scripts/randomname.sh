#!/bin/bash

# Renames a file with a new random 16 characters name
file=$1
filename=$(basename $1)
randomname=$(head /dev/urandom | tr -cd 'a-zA-Z0-9' | head -c 16)
case $filename in
    *.*)
        extension=${filename#*.}
        mv $filename $randomname.$extension
        #echo $randomname.$extension
        ;;
    *)
        extension=""
        #echo $randomname
        mv $filename $randomname
        ;;
esac

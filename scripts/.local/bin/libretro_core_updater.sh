#! /bin/sh

CORES_DIR="$HOME/Games/retroarch/cores"
INFO_DIR="$HOME/Games/retroarch/cores/info"

cores="citra dolphin swanstation fbneo flycast genesis_plus_gx mednafen_ngp mednafen_pce_fast mednafen_vb melonds mesen mgba mupen64plus_next nxengine pokemini ppsspp sameboy snes9x"

mkdir -p "$CORES_DIR"
mkdir -p "$INFO_DIR"
mkdir -p "${XDG_CACHE_HOME:-$HOME/.cache}/libretro-cores"
cd "${XDG_CACHE_HOME:-$HOME/.cache}/libretro-cores" || exit 1

# Download specified cores from latest nightly
for core in $cores; do
    wget -A zip https://buildbot.libretro.com/nightly/linux/x86_64/latest/"$core"_libretro.so.zip
done

# Download core info files
wget -A zip https://buildbot.libretro.com/assets/frontend/info.zip

# Unzip everything
unzip "*.zip"

# Copy the cores and info files to their respective directories
cp -v *.so "$CORES_DIR"
cp -v *.info "$INFO_DIR"

# Cleanup
cd ../
rm -rf libretro-cores
echo "Done!"

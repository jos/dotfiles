#!/bin/sh

# This script removes duplicates from the bash history
# This way, the history file will have as many different
# commands as possible
# https://medium.com/@dblume/have-only-unique-lines-in-your-bash-history-941e3de68c03

T=$(mktemp)
tac "${HISTFILE:-$HOME/.bash_history}" | awk '!x[$0]++' | tac > "$T"
mv "$T" "${HISTFILE:-$HOME/.bash_history}"

#!/bin/sh
# Increase/decrease, or toggle the mute status,
# and send the updated value to the xob named pipe

usage() {
    cat <<EOF
Usage: $0 -i arg | -d arg | -m | -h

where:
    -i increase volume arg%
    -d decrease volume arg%
    -m toggle the mute status
    -h show this help message
EOF
    exit 0
}

send_notification() {
    volume="$(pamixer --get-volume)"
    notify-send -t 1500 -i audio-volume-high \
        -h string:x-canonical-private-synchronous:volume.sh \
        -h string:hlcolor:#bd93f9 \
        -h int:value:"$volume" \
        "Volume: $volume%"
}

send_mute_notification() {
    notify-send -t 1500 -i audio-volume-muted \
        -h string:x-canonical-private-synchronous:volume.sh \
        "Volume: Muted"
}

while getopts ":i:d:mh" opt; do
    case ${opt} in
        h)  # show the usage
            usage
            ;;

        i)  # increase volume
            pamixer --allow-boost --unmute --increase $OPTARG
            send_notification
            ;;
    
        d)  # decrease volume
            pamixer --allow-boost --unmute --decrease $OPTARG
            send_notification
            ;;
    
        m)  # toggle mute
            pamixer --toggle-mute
            if $(pamixer --get-mute); then
                send_mute_notification
            else
                send_notification
            fi
            ;;

        \?) # unknown optarg
            echo "Invalid option: -$OPTARG" 1>&2
            exit 1
            ;;

        : )  # optarg without argument
            echo "Option -"$OPTARG" requires an argument." 1>&2
            exit 1
            ;;
    esac
done

#!/bin/sh

usage() {
    cat <<EOF
Usage: ${0##*/} [OPTIONS]

Where OPTIONS can be one or more of the following:
    -W | --window    take a screenshot of the current window
    -D | --desktop   take a screenshot of the entire desktop
    -S | --selection take a screenshot of the selected area
    -w | --webp      save the image as lossless webp
    -c | --clipboard copy the image to the clipboard via xclip
    -h | --help      show this help message

The window, desktop and selection flags are mutually exclusive
and only one of them at the same time may be used. They are also
mandatory, meaning that one of them must be called when running
the script, with or without the webp and clipboard flags.

The webp flag, while not mutually exclusive with clipboard, will
be ignored if the latter was used.
EOF
    exit 0
}

send_notification() {
    summary="$1"
    body="$2"
    [ -n "$3" ] && icon="$3" || icon="${XDG_DATA_HOME:-$HOME/.local/share}/icons/camera.png"
    notify-send -t 5000 -i "$icon" -u normal \
        -h string:x-canonical-private-synchronous:shot.sh \
        "$summary" "$body"
}

SELECT=false # Whether or not the selection flag was enabled
CLIPBOARD=false; WEBP=false # Same as above, but for clipboard and webp flags
is_flag=false

# getopts does not support long options, so we convert them to short options.
for arg in "$@"; do
    shift
    case "$arg" in
        --desktop) set -- "$@" '-D' ;;
        --selection) set -- "$@" '-S' ;;
        --window) set -- "$@" '-W' ;;
        --clipboard) set -- "$@" '-c' ;;
        --webp) set -- "$@" '-w' ;;
        --help) set -- "$@" '-h' ;;
        *) set -- "$@" "$arg" ;;
    esac
done

while getopts "DSWchw" opt; do
    case ${opt} in
        D) $is_flag && usage
           is_flag=true
           WINDOW="-window root"
           ;;
        S) $is_flag && usage
           SELECT=true; is_flag=true
           WINDOW=""
           ;;
        W) $is_flag && usage
           is_flag=true
           WINDOW="-window \"$(xdo id)\""
           ;;
        c) CLIPBOARD=true ;;
        w) WEBP=true ;;
        h) usage ;;
        \?) usage ;;
        :) usage ;;
    esac
done

# Make sure one of the main flags (D|W|S) was enabled
$is_flag || usage

# Assemble the final command based on the enabled flags
if ! $CLIPBOARD; then
    $WEBP && webp_options="-define webp:lossless=true" || webp_options=""
    $WEBP && name_extension="webp" || name_extension="png"
    name="$HOME/Pictures/screenshots/screenshot_$(date +%Y%m%d_%H%M%S).$name_extension"
    xclip_options=""

    [ -d "$HOME/Pictures/screenshots" ] || mkdir -p "$HOME/Pictures/screenshots"

    summary="${name##*/}"
    body="Saved to ~/Pictures/screenshots"
    $WEBP && icon="" || icon="$name"
else
    webp_options=""
    name="png:-"
    xclip_options=" | xclip -t 'image/png' -selection clipboard"

    summary="Screenshot!"
    body="Copied to the clipboard"
    icon=""
fi

# if the selection flag is enabled, show the cursor in case it was hidden
$SELECT && xdo pointer_motion -x +0 -y +0
eval "import $WINDOW $webp_options $name $xclip_options"
send_notification "$summary" "$body" "$icon"

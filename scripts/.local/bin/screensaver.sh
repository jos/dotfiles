#!/bin/sh

# Run asciiquarium as a screensaver, then wait to be killed and
# kill asciiquarium in the process

trap 'exit 0' TERM INT
trap "xdo kill -N aquarium >/dev/null 2>&1" EXIT

is_playing=$(pactl list | grep -c "State: RUNNING")
active_wid=$(xprop -root | awk '$1 ~ /_NET_ACTIVE_WINDOW/ { print $5 }')
is_fullscreen=$(xprop -id "$active_wid" | grep -c _WM_STATE_FULLSCREEN)

if [ "$is_fullscreen" -eq 0 ] && [ "$is_playing" -eq 0 ]; then
    st -c aquarium -e asciiquarium >/dev/null 2>&1 &
else
    xset s reset
fi

wait

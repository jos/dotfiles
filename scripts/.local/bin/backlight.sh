#!/bin/sh
# Increase/decrease backlight,
# and send the updated value to the xob named pipe

usage() {
    cat <<EOF
Usage: $0 -i arg | -d arg | -h

where:
    -i increase backlight arg%
    -d decrease backlight arg%
    -h show this help message
EOF
    exit 0
}

send_notification() {
    if [ -x "$(command -v xob)" ]; then
       light -G | awk '{print int($1+0.5)}' >> /tmp/xob_backlight
    else
       notify-send -t 1500 -i brightnesssettings \
           -h string:x-canonical-private-synchronous:backlight.sh \
           -h string:hlcolor:#bd93f9 \
           -h int:value:"$(light -G)" \
           "Brightness: $(light -G)%"
    fi
}

while getopts ":i:d:h" opt; do
    case ${opt} in
        h)  # show the usage
            usage
            ;;

        i)  # increase backlight
            light -A $OPTARG
            send_notification
            ;;
    
        d)  # decrease volume
            light -U $OPTARG
            send_notification
            ;;
    
        \?) # unknown optarg
            echo "Invalid option: -$OPTARG" 1>&2
            exit 1
            ;;

        : )  # optarg without argument
            echo "Option -"OPTARG" requires an argument." 1>&2
            exit 1
            ;;
    esac
done

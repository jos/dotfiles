#!/bin/sh
# Toggle the currently focused window via --dynamic, or select the class,
# instance or title to search for via --named,  plus the command
# that will be run (for example 'st -c scratchpad' or 'steam'), and toggle that.
# Its main use case is to create scratchpad-like windows that will be hidden or shown
# with a keybind, but GUI applications, or applications that have their own title
# or class should also be supported, although they are more hit or miss and could
# require extra steps.

# Temporary file to store the hidden window IDs
ISHIDDEN_FILE="/tmp/ishidden.wid"


usage() {
    cat <<EOF
Usage: ${0##*/} [--named <name> -- <command>] [--dynamic <option>] [-h]

* A --named scratchpad targets applications that have a specific class,
  instance or title name.

  If the "--named" option is specified, an additional string (name)
  must be passed as an argument. This string will be used to search
  for an existing window by class, instance or title name, in that order.
  
  The "--" must be used to mark the end of the options and the start of
  the command that will be executed.

  Finally, <command> is the command responsible for spawning a window with
  a class, instance or title matching the name string.


* A --dynamic scratchpad can target any application, as long as it's running.

  If the "--dynamic" option is specified, an extra argument must be passed
  in the form of "push" or "pop".

  If the "push" option is supplied, the currently focused window will be
  hidden and turned temporarily into a scratchpad, while populating the
  dynamic scratchpad stack.

  On the other hand, if the script is called with the "pop" option, the
  first dynamic pad in the stack will be brought back, in a FIFO manner,
  and will then be removed from the stack.


* -h prints this help and exits.


The options shouldn't be mixed together, but if they were,
only the first one of them will be accepted.
EOF
    exit 0
}


detect_existing_scratchpads() {
    [ -f "$ISHIDDEN_FILE" ] || touch "$ISHIDDEN_FILE"

    # Check if a scratchpad already exists inside the file
    wid="$(grep "$1" "$ISHIDDEN_FILE" | sed 's/ .*//' | head -n 1)"

    # Remove the entry if the window was closed while it was hidden.
    xprop -id "$wid" >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        [ -n "$wid" ] && { sed -i "/$wid/d" "$ISHIDDEN_FILE"; wid=""; }
    fi
}

show_scratchpads() {
    xdo show "$1"
    sed -i "/$1/d" "$ISHIDDEN_FILE"
}

hide_scratchpads() {
    xdo hide "$1"
    echo "$1 ($2)" >> "$ISHIDDEN_FILE"
}

# Dynamic scratchpads
dynamic() {

    if [ "$1" = "push" ]; then
        wid="$(xdo id)"
        [ -n "$wid" ] && hide_scratchpads "$wid" "dynamic"
    fi

    if [ "$1" = "pop" ]; then
        while [ "$(grep "dynamic" "$ISHIDDEN_FILE")" ]; do
            detect_existing_scratchpads "dynamic"
            [ -n "$wid" ] && break
        done
        [ -n "$wid" ] && show_scratchpads "$wid"
    fi
}

# Named scratchpads
named() {
    name="$OPTARG"
    if [ "$name" = "--" ]; then
        printf "%s\n" "A name is required (see -h)."
        exit 1
    fi

    # Shift arguments
    while [ "$1" != "--" ]; do
        shift
    done && shift

    detect_existing_scratchpads "$name"

    # If no appropriate named scratchpads could be found, check if 
    # a window with the specific class exists, then instance and name
    [ -z "$wid" ] && wid=$(xdo id -N "$name" | head -n1)
    [ -z "$wid" ] && wid=$(xdo id -n "$name" | head -n1)
    [ -z "$wid" ] && wid=$(xdo id -a "$name" | head -n1)
    
    # If a valid win id was found, toggle, otherwise spawn a new pad
    if [ -n "$wid" ]; then
        if grep -qw "$wid" "$ISHIDDEN_FILE"; then
            show_scratchpads "$wid"
        else
            hide_scratchpads "$wid" "$name"
        fi
    else
        # Verify that the command passed as argument exists and run it
        if [ ! -x "$(command -v "$1")" ]; then
            printf "%s\n" "The command must be a valid one (see -h)."
            exit 1
        fi

        "$@" >/dev/null 2>&1 &
    fi
}


# getopts does not support long options, so we convert them to short options.
for arg in "$@"; do
    shift
    case "$arg" in
        --dynamic) set -- "$@" '-d' ;;
        --named) set -- "$@" '-n' ;;
        *) set -- "$@" "$arg" ;;
    esac
done

while getopts "hd:n:" opt; do
    case ${opt} in
        d) case "${OPTARG}" in push | pop) ;; *) usage ;; esac
           dynamic "${OPTARG}"; exit ;;
        n) named "$@"; exit ;;
        h) usage ;;
        \?) usage ;;
        :) usage ;;
    esac
done

# if the file is empty, exit
if [ ! -s "$ISHIDDEN_FILE" ]; then
    printf "%s\n" "There are no scratchpads running (see -h)."
    exit
fi

# show the hidden scratchpads on dmenu and raise the selected one
# or raise it without asking if there's only one
if [ "$(wc -l < "$ISHIDDEN_FILE")" -eq 1 ]; then
    show_scratchpads "$(cat $ISHIDDEN_FILE | sed 's/\s.*//')"
else
    SELECTION="$(cat "$ISHIDDEN_FILE" | dmenu)"
    [ -n "$SELECTION" ] || exit 0
    wid="$(echo $SELECTION | sed 's/\s.*//')"
    show_scratchpads "$wid"
fi

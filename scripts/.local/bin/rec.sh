#!/bin/sh
# TODO: make the recorder options adjustable from the CLI.


usage() {
    cat <<EOF
Usage: ${0##*/} [--record] [--replay start|save|stop]

OPTIONS:
--record  Toggle recording the entire screen.

--replay  Start/Stop gpu-screen-recorder in replay mode,
            or save a ~30 seconds replay from what just happened
            on the screen.
EOF
    exit 0
}


record_screen() {
    # use the replay mode exclusive -r option to determine which process to kill
    PID="$(ps -ef | awk '/gpu-screen-recorder/ && !/-r /' | grep -v awk | awk '{print $2}')"
    [ -n "$PID" ] && kill -s INT "$PID" && \
        notify-send -t 3000 'GPU Screen Recorder' "Stopped recording" && exit 0

    active_sink="$(pactl get-default-sink).monitor"
    mkdir -p "$HOME/Videos"
    video="$HOME/Videos/$(date +"Video_%Y-%m-%d_%H.%M.%S.mp4")"
    gpu-screen-recorder -w screen -c mp4 -f 60 -a "$active_sink" -o "$video" >/dev/null 2>&1 &
}

replay_mode() {
    # use the replay mode exclusive -r option to determine which process to kill
    PID="$(ps -ef | awk '/gpu-screen-recorder/ && /-r /' | grep -v awk | awk '{print $2}')"

    case "$1" in
        start)
            if [ -z "$PID" ]; then
                active_sink="$(pactl get-default-sink).monitor"
                out="$HOME/Videos"
                mkdir -p "$out"
                gpu-screen-recorder -w screen -c mp4 -f 60 -a "$active_sink" -r 30 -o "$out" >/dev/null 2>&1 &
                notify-send -t 3000 -- 'GPU Screen Recorder' "Replay mode started"
            fi
            ;;
        stop)
            [ -n "$PID" ] && kill -s INT "$PID" && \
                notify-send -t 3000 -- 'GPU Screen Recorder' "Replay mode stopped"
            ;;
        save)
            [ -n "$PID" ] && kill -s USR1 "$PID" && \
                notify-send -t 3000 -- 'GPU Screen Recorder' "Replay saved"
            ;;
    esac
}


# getopts does not support long options, so we convert them to short options.
for arg in "$@"; do
    shift
    case "$arg" in
        --record) set -- "$@" '-c' ;;
        --replay) set -- "$@" '-p' ;;
        *) set -- "$@" "$arg" ;;
    esac
done

while getopts "cp:" opt; do
    case ${opt} in
        c) record_screen; exit ;;
        p) case "${OPTARG}" in start | stop | save) ;; *) usage ;; esac
           replay_mode "${OPTARG}"; exit ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))
